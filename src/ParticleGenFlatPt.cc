#include "ParticleGenFlatPt.h"

#include "g4main/PHG4Particle.h"    // for PHG4Particle
#include "g4main/PHG4Particlev2.h"
#include "g4main/PHG4InEvent.h"

#include <phool/getClass.h>

#include <gsl/gsl_rng.h>     // for gsl_rng_uniform_pos

#include <cmath>
#include <iostream>          // for operator<<, endl, basic_ostream, basic_o...
#include <vector>            // for vector, vector<>::iterator

class PHCompositeNode;

using namespace std;

ParticleGenFlatPt::ParticleGenFlatPt(const string &name)
  : PHG4ParticleGenerator(name)
  , transv_mom_min(0.0)
  , transv_mom_max(10.0)
  , momSet(false)
  , transvMomSet(false)
{
  return;
}

void ParticleGenFlatPt::set_mom_range(const double min, const double max)
{
  if (!transvMomSet) {
	  mom_min = min;
	  mom_max = max;
	  momSet = true;
  } else {
	  std::cout << "Transverse momentum already set. Can't set both transverse momentum and full momentum. Only transverse value will be used." << std::endl;
  }
  return;
}

/*Setting transverse momentum*/
void ParticleGenFlatPt::set_transv_mom_range(const double min, const double max)
{
  if (!momSet) {
	  transv_mom_min = min;
	  transv_mom_max = max;
	  transvMomSet = true;
  } else {
	  std::cout << "Momentum already set. Can't set both transverse momentum and full momentum. Only full momentum value will be used." << std::endl;
  }
  return;
}

int ParticleGenFlatPt::process_event(PHCompositeNode *topNode)
{
  PHG4InEvent *ineve = findNode::getClass<PHG4InEvent>(topNode, "PHG4INEVENT");

  if (!ReuseExistingVertex(topNode))
  {
    vtx_z = (z_max - z_min) * gsl_rng_uniform_pos(RandomGenerator) + z_min;
  }
  int vtxindex = ineve->AddVtx(vtx_x, vtx_y, vtx_z, t0);

  vector<PHG4Particle *>::iterator iter;
  for (iter = particlelist.begin(); iter != particlelist.end(); ++iter)
  {
    PHG4Particle *particle = new PHG4Particlev2(*iter);
    SetParticleId(particle, ineve);
    
    double mom = 0.0;
    double eta = 0.0;
    double phi = 0.0;
    double pt = 0.0;
    
    if (!transvMomSet) { //If the momentum is set. Standard behaviour. Flat in p.
		mom = (mom_max - mom_min) * gsl_rng_uniform_pos(RandomGenerator) + mom_min;
		eta = (eta_max - eta_min) * gsl_rng_uniform_pos(RandomGenerator) + eta_min;
		phi = (phi_max - phi_min) * gsl_rng_uniform_pos(RandomGenerator) + phi_min;
		pt = mom / cosh(eta);
	} else {
		//If we've set he transverse momentum: want flat in pT.
		pt = (transv_mom_max - transv_mom_min) * gsl_rng_uniform_pos(RandomGenerator) + transv_mom_min;
		eta = (eta_max - eta_min) * gsl_rng_uniform_pos(RandomGenerator) + eta_min;
		phi = (phi_max - phi_min) * gsl_rng_uniform_pos(RandomGenerator) + phi_min;
		mom = pt*cosh(eta);
	}
	
	//std::cout << "Eta: " << eta_max << " " << eta_min << std::endl;
	//std::cout << "Phi: " << phi_max << " " << phi_min << std::endl;
	//std::cout << "Z: " << z_max << " " << z_min << std::endl;
	
	//This can then all remain the same
    particle->set_e(mom);
    particle->set_px(pt * cos(phi));
    particle->set_py(pt * sin(phi));
    particle->set_pz(pt * sinh(eta));
    // update internal particle list with changed momenta
    // needed for correct printout of particle kinematics
    // in ParticleGenFlatPt::Print()
    (*iter)->set_e(particle->get_e());
    (*iter)->set_px(particle->get_px());
    (*iter)->set_py(particle->get_py());
    (*iter)->set_pz(particle->get_pz());
    ineve->AddParticle(vtxindex, particle);
  }
  if (Verbosity() > 0)
  {
    ineve->identify();
  }
  return 0;
}

void ParticleGenFlatPt::Print(const std::string &what) const
{
  cout << "ParticleGenFlatPt settings:" << endl;
  cout << "z_min, z_max: " << z_min << "/" << z_max << endl;
  cout << "eta_min, eta_max: " << eta_min << "/" << eta_max << endl;
  cout << "phi_min, phi_max: " << phi_min << "/" << phi_max << endl;
  cout << "mom_min, mom_max: " << mom_min << "/" << mom_max << endl;
  cout << "transv_mom_min, transv_mom_max: " << mom_min << "/" << mom_max << endl;
  PrintParticles(what);
  return;
}
