// Tell emacs that this is a C++ source
//  -*- C++ -*-.
#ifndef G4MAIN_ParticleGenFlatPt_H
#define G4MAIN_ParticleGenFlatPt_H

#include "g4main/PHG4ParticleGenerator.h"
#include "g4main/PHG4ParticleGeneratorBase.h"

#include <string>                       // for string

class PHCompositeNode;

/* Generates flat in pT instead of p
* H. Wennlöf, September 2020
*/
class ParticleGenFlatPt : public PHG4ParticleGenerator
{
 public:
  ParticleGenFlatPt(const std::string &name = "PGENERATOR");
  virtual ~ParticleGenFlatPt() {}

  int process_event(PHCompositeNode *topNode);
  void set_mom_range(const double mom_min, const double mom_max);
  void set_transv_mom_range(const double mom_min, const double mom_max);
  void Print(const std::string &what = "ALL") const;

 protected:
  double transv_mom_min;
  double transv_mom_max;
  bool momSet;
  bool transvMomSet;
};

#endif
