# Fun4All_particleGenFlatPt

Particle generator giving flat pT, for Fun4All.

Subclass of PHG4ParticleGenerator. Most things copied from PHG4ParticleGenerator, but modified in the necessary ways. Function set_transv_mom_range added, but also kept set_mom_range, so it can do both.


Using the Fun4All structure of things.

Recompile by setting up the Fun4All setup;
`singularity shell -B /home  -B /cvmfs /cvmfs/eic.opensciencegrid.org/singularity/rhic_sl7_ext.simg`

`source /cvmfs/eic.opensciencegrid.org/x8664_sl7/opt/fun4all/core/bin/eic_setup.sh -n`

`source /cvmfs/eic.opensciencegrid.org/x8664_sl7/opt/fun4all/core/bin/setup_local.sh $HOME/myinstall`

then going into source/build and running

`../autogen.sh --prefix=$HOME/myinstall`

and

`make install`

Then `source /cvmfs/eic.opensciencegrid.org/x8664_sl7/opt/fun4all/core/bin/setup_local.sh $HOME/myinstall`.


To use it in Fun4All, one needs to do 
`#include <particlegenflatpt/ParticleGenFlatPt.h>`
`R__LOAD_LIBRARY(libparticlegenflatpt.so)`

And then something like

    ParticleGenFlatPt * gen = new ParticleGenFlatPt();
	gen->set_name(std::string("pi+"));
	gen->set_vtx(0,0,0);			// Vertex generation range
	//gen->set_mom_range(0.0, 30.0);		// Momentum generation range in GeV/c
	gen->set_transv_mom_range(0.0, 30.0);		// Transverse momentum generation range in GeV/c. Flat in transverse momentum
	gen->set_z_range(0.0, 0.0); //THIS NEEDS TO BE SET, TO NOT DEFAULT TO -10 TO 10
	gen->set_eta_range(-4.0, 4.0);
	gen->set_phi_range(0.0, 2.0*TMath::Pi());
